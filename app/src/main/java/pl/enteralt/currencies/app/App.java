package pl.enteralt.currencies.app;

import android.app.Application;

import pl.enteralt.currencies.injector.component.AppComponent;
import pl.enteralt.currencies.injector.component.DaggerAppComponent;
import pl.enteralt.currencies.injector.module.AppModule;

/**
 * Base Application class
 */
public class App extends Application {

    /**
     * Dependencies for application lifecycle
     */
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeComponents();
    }

    /**
     * Initialize components for Application class
     */
    private void initializeComponents() {

        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    /**
     * @return {@link AppComponent}
     */
    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
