package pl.enteralt.currencies.injector.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Scope for whole application lifecycle
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope{

}
