package pl.enteralt.currencies.injector.module;

import dagger.Module;
import dagger.Provides;
import pl.enteralt.currencies.app.App;
import pl.enteralt.currencies.injector.scope.AppScope;

/**
 * Dagger2 Module for Application
 */
@Module
public class AppModule {

    /**
     * Application class
     */
    private final App application;

    public AppModule(App application) {
        this.application = application;
    }

    /**
     * Provide Application dependency
     * @return Application class object
     */
    @AppScope
    @Provides
    public App provideApp(){
        return application;
    }
}
