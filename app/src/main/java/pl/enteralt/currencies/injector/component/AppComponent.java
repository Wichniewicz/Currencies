package pl.enteralt.currencies.injector.component;

import dagger.Component;
import pl.enteralt.currencies.app.App;
import pl.enteralt.currencies.injector.module.AppModule;
import pl.enteralt.currencies.injector.scope.AppScope;

/**
 * Component for Application
 */
@AppScope
@Component(modules = {AppModule.class})
public interface AppComponent {

    /**
     * @return Application dependency
     */
    App app();

    /**
     * Inject dependencies into {@link App}
     * @param app {@link App}
     */
    void inject(App app);
}
